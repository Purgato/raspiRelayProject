package it.sensorepioggia.raspberry.lp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import it.sensorepioggia.raspberry.lp.PioggiaApplication;

@SpringBootApplication
public class PioggiaApplicationTests {

	public static void main(String[] args) {
		SpringApplication.run(PioggiaApplication.class, args);
	}
}
