package it.sensorepioggia.raspberry.lp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;

@SpringBootApplication
public class PioggiaApplication   implements GpioPinListenerDigital {

	public static void main(String[] args) {
		SpringApplication.run(PioggiaApplication.class, args);
	}


@Override
public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent event) {
    // display pin state on console
    System.out.println(" --> GPIO PIN STATE CHANGE: " + event.getPin() + " = "
            + event.getState());
}

}