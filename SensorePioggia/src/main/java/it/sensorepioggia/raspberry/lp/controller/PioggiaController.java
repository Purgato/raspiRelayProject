package it.sensorepioggia.raspberry.lp.controller;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.wiringpi.Gpio;

@Controller
public class PioggiaController {
	

	public static GpioPinDigitalOutput pin11;
	GpioController gpio = GpioFactory.getInstance();
	GpioPinDigitalInput myButton = gpio.provisionDigitalInputPin(RaspiPin.GPIO_00);
	String statoPioggia = null;
	
	@GetMapping("/")
	public String index(Model model)
	{

			int stateDigitalRead = Gpio.digitalRead(0);
			if(stateDigitalRead == 1) {
				statoPioggia = "Non piove!!!";
			}
			else {
				statoPioggia = "Piove!!!";
			}

			
			boolean buttonPressed = myButton.isHigh();
			PinState myButtonState = myButton.getState();
			
			System.out.println("------------------");
			System.out.println("myButtonState.getValue()");
			System.out.println(myButtonState.getValue());
			
			System.out.println("Gpio.digitalRead(11)");
			System.out.println(stateDigitalRead);
			System.out.println("------------------");
			
			
			System.out.println("------------------");
			System.out.println("myButton.isHigh();");
			System.out.println(buttonPressed);
			
			model.addAttribute("stato", statoPioggia);
		return "index";
	}
	
}
