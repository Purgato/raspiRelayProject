RaspiRelayProject

Questo progetto è stato ideato per automatizzare un sistema di irrigazione casalinga mediante Raspberry Pi 2/3 B (anche per altri modelli mappando opportunamente i pin GPIO),
tuttavia è utilizzabile per qualsiasi sistema agganciato ai relè.

Le tecnologie utilizzate sono:

- Pi4j
- Springboot
- Mysql: per il tracciamento delle attività temporizzate
- Quartz Scheduler: per la schedulazione degli interventi

Il progetto è in fase di sviluppo e per ora prevede l'utilizzo di due relè 5V (Imax=250A) alimentati direttamente da raspberry (per più unità è preferibile un 
alimentazione esterna).
Lato Frontend, il sistema embedded espone una dashboard dalla quale effettuare i settaggi sugli impianti connessi, temporizzare gli interventi ed eventualmente modificarli/eliminarli. 

![alt text](https://gitlab.com/Purgato/raspiRelayProject/raw/master/index2.jpg)


Lato Backend, l'accesso alla porta gpio del raspberry avviene mediante le librerie Java Pi4j, la temporizzazione prevede l'accensione/spegnimento degli impianti
a una determinata data e la possibilità di ripetere l'intervento giornalmente. Inoltre sono disponibili degli end point per effettuare un controllo mendiate 
chiamate rest, per una possibile integrazione con altri servizi.


![alt text](https://gitlab.com/Purgato/raspiRelayProject/raw/master/form.PNG)


Lato Database, gli interventi temporizzati vengono inseriti nella tabella "irrigazione", cosi facendo in caso di riavvio improvviso del Raspberry le schedulazioni
non andranno perse.

Il job quarz effettua un controllo su db ogni 5secondi per verificare la presenza o meno di interventi schedulati.


####### MYSQL - DOCKER ######

Mysql Dockerizzato: 

- docker pull tobi312/rpi-mysql

- mkdir -p /home/pi/.local/share/mysql

- sudo docker run -d -p 3306:3306 -v /home/-----UTENTE----/.local/share/mysql:/var/lib/mysql --name mysql -e MYSQL_ROOT_PASSWORD=***** -e MYSQL_USER=irrigazione -e MYSQL_PASSWORD=***** -e MYSQL_DATABASE=irrigazione -d mysql/mysql-server:5.5

oppure

sudo docker run -d -p 3306:3306 -v /home/pi/.local/share/mysql:/var/lib/mysql --name mysql -e MYSQL_ROOT_PASSWORD=***** -e MYSQL_USER=irrigazione -e MYSQL_PASSWORD=***** -e MYSQL_DATABASE=db_example -d tobi312/rpi-mysql

Per i logs:

- sudo docker logs -f {id container}

Per "sql plus"

docker exec -it mysql bash
mysql -u root -p irrigazione

######## MYSQL #######
sudo apt-get install mysql-server && sudo apt-get install mysql-client

mysql --host=[INSTANCE_IP] --user=root --password

CREATE USER 'newuser'@'localhost' IDENTIFIED BY 'password';

GRANT ALL PRIVILEGES ON * . * TO 'newuser'@'localhost';


######### DOCKER UTILS ####################################

1- lista container

sudo docker ps -a -q

2- lista immagini

sudo docker images

3- Ferma tutti i container
docker stop $(sudo docker ps -a -q)

4- Rimuovi tutti i container
docker rm $(sudo docker ps -a -q)


Cancella tutto!!!
docker system prune

Cancella e ferma tutto
docker system prune -a
##############################################


CRON RASPBERRY

crontab -u pi -e
@reboot sh /home/pi/ sh mysh.sh

