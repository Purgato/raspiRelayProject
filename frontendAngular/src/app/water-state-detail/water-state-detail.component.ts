import { Component, OnInit, Input } from '@angular/core';
import { WaterStateInterface } from '../interfaces/water-state-interface';

@Component({
  selector: 'app-water-state-detail',
  templateUrl: './water-state-detail.component.html',
  styleUrls: ['./water-state-detail.component.css']
})
export class WaterStateDetailComponent implements OnInit {

  @Input() waterState: WaterStateInterface;

  constructor() { }

  ngOnInit() {
  }

}
