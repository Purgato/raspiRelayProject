import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WaterStateDetailComponent } from './water-state-detail.component';

describe('WaterStateDetailComponent', () => {
  let component: WaterStateDetailComponent;
  let fixture: ComponentFixture<WaterStateDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WaterStateDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WaterStateDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
