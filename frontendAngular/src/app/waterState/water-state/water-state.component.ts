import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { ConnectionsService } from '../../connections/connections.service';
import { WaterStateInterface } from '../../interfaces/water-state-interface';

@Component({
  selector: 'tr[app-water-state]',
  templateUrl: './water-state.component.html',
  styleUrls: ['./water-state.component.css']
})
export class WaterStateComponent implements OnInit {
  
  constructor(private service : ConnectionsService) { }
  
  @Input('waterStateInput') waterState: WaterStateInterface; //indicando che waterState è di tipo WaterStateInterface per l'istanza waterState saranno visibili tt le sue proprietà e altri metodi
  @Output() onDeleteWaterState =new EventEmitter();
  @Output() onSelectWaterState = new EventEmitter();

  deleteSchedulazione(){
    this.onDeleteWaterState.emit(this.waterState);
  }

  updateSchedulazione(waterState){
    this.onSelectWaterState.emit(this.waterState);
  }





  ngOnInit() {
  }

}
