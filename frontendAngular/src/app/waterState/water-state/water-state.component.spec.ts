import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WaterStateComponent } from './water-state.component';

describe('WaterStateComponent', () => {
  let component: WaterStateComponent;
  let fixture: ComponentFixture<WaterStateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WaterStateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WaterStateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
