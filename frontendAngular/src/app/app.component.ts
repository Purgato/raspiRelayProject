import { Component, OnInit} from '@angular/core';
import { WaterStateInterface } from './interfaces/water-state-interface';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'title';
  impianto1Url = 'http://localhost:9999/impianto1Fe';
  impianto2Url = 'http://localhost:9999/impianto2Fe';
  waterStateSelected: WaterStateInterface;


  impianto1(event) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", this.impianto1Url, false); // true for asynchronous 
    xmlHttp.send(null);
    alert("Clicked impianto1: " + xmlHttp.responseText);
  }
  impianto2(event) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "GET", this.impianto2Url, false ); // false for synchronous request
    xmlHttp.send(null);
    alert("Clicked impianto2: " + xmlHttp.responseText);
  }

  updateWaterState(waterState: WaterStateInterface){
    this.waterStateSelected = waterState;
  }


  timer(event) {

  }

  constructor() {}  
  ngOnInit() {}

}
