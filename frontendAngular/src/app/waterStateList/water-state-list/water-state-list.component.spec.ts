import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WaterStateListComponent } from './water-state-list.component';

describe('WaterStateListComponent', () => {
  let component: WaterStateListComponent;
  let fixture: ComponentFixture<WaterStateListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WaterStateListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WaterStateListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
