import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ConnectionsService } from '../../connections/connections.service';
import { WaterStateInterface } from '../../interfaces/water-state-interface';

@Component({
  selector: 'app-water-state-list',
  templateUrl: './water-state-list.component.html',
  styleUrls: ['./water-state-list.component.css']
})
export class WaterStateListComponent implements OnInit {

  waterStateList: WaterStateInterface[] = []; //essendo di tipo WaterStateInterface[] posso accedere a una serie di metodi applicabili all'array di tale tipo

  @Output() updateWaterState = new EventEmitter<WaterStateInterface>(); //<WaterStateInterface> 


  constructor(private service : ConnectionsService) {
    

   }
   onDeleteWaterState(waterState: WaterStateInterface) {
    this.service.deleteSchedulazione(waterState);
   }

   onSelectWaterState(waterState: WaterStateInterface){
     this.updateWaterState.emit(waterState);
   }

  ngOnInit() {
    this.waterStateList = this.service.getWaterState();
  }

}
