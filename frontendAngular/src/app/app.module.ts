import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ConnectionsService } from './connections/connections.service';
import { AppComponent } from './app.component';
import { WaterStateListComponent } from './waterStateList/water-state-list/water-state-list.component';
import { WaterStateComponent } from './waterState/water-state/water-state.component';
import { WaterStateDetailComponent } from './water-state-detail/water-state-detail.component';
import { FormsModule } from '@angular/forms'

@NgModule({
  declarations: [
    AppComponent,
    WaterStateListComponent,
    WaterStateComponent,
    WaterStateDetailComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [
    ConnectionsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
