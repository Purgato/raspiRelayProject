import { Injectable } from '@angular/core';
import { WaterStateInterface } from '../interfaces/water-state-interface';
@Injectable({
  providedIn: 'root'
})
export class ConnectionsService {
  waterStateList: Array<WaterStateInterface> = [{   //Array di elementi dichiarati nell'interfaccia WaterStateInterface (oppure WaterStateInterface[] )
    impianto1:1,
    impianto2:2,
    dataInizio:'15/12/2018',
    orarioInizio:'orarioInizio',
    tempo:1
}]
 
  getWaterState(){
    return this.waterStateList;
  }

  deleteSchedulazione(stato){
    let index = this.waterStateList.indexOf(stato);
    if(index>=0){
      this.waterStateList.splice(index,1);
    }
  }

  constructor() { }
}
