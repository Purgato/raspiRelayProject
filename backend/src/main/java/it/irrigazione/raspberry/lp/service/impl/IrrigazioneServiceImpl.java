package it.irrigazione.raspberry.lp.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
import it.irrigazione.raspberry.lp.model.WaterState;
import it.irrigazione.raspberry.lp.repository.RepositoryIrr;
import it.irrigazione.raspberry.lp.service.IrrigazioneService;

@Service
public class IrrigazioneServiceImpl implements IrrigazioneService {

	public static GpioPinDigitalOutput pin1;
	public static GpioPinDigitalOutput pin2;

	@Autowired
	RepositoryIrr repository;
	private static final Logger logger = LogManager.getLogger(IrrigazioneServiceImpl.class);

	public List<GpioPinDigitalOutput> initializeRaspberry() {
//		 if(pin1==null) {
//			 GpioController gpio = GpioFactory.getInstance();
//			 pin1 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_01, "imianto1",
//			 PinState.HIGH);
//		 }
//		 if(pin2==null) {
//			 GpioController gpio = GpioFactory.getInstance();
//			 pin2 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_04, "My Rele",
//			 PinState.HIGH);
//		 }
		List<GpioPinDigitalOutput> state = new ArrayList<>();
		state.add(pin1);
		state.add(pin2);
		return state;
	}

	public void switchImpianto1() {
		pin1.toggle();
	}

	public void switchImpianto2() {
		pin2.toggle();
	}

	public void azionaImpianto(WaterState water) {
//		if (water.getImpianto1() == 0)
//			pin1.low();
//		else
//			pin1.high();
//
//		if (water.getImpianto2() == 0)
//			pin2.low();
//		else
//			pin2.high();
	}

	public void spegniImpianto(WaterState water) {
//		pin1.high();
//		pin2.high();
	}

	public static GpioPinDigitalOutput getPin1() {
		return pin1;
	}

	public static void setPin1(GpioPinDigitalOutput pin1) {
		IrrigazioneServiceImpl.pin1 = pin1;
	}

	public static GpioPinDigitalOutput getPin2() {
		return pin2;
	}

	public static void setPin2(GpioPinDigitalOutput pin2) {
		IrrigazioneServiceImpl.pin2 = pin2;
	}

	@Override
	public List<WaterState> listaTot() {
		List<WaterState> lista = (List<WaterState>) repository.findAll();
		return lista;
	}

	@Override
	public void eliminaSchedulazione(Long id) {
		repository.delete(id);
	}

	@Override
	public void createSchedulazione(int impianto1, int impianto2, Date data, String ripeti, int tempo) {
		WaterState newState = new WaterState();
		newState.setDataInizio(data);
		newState.setImpianto1(impianto1);
		newState.setImpianto2(impianto2);
		newState.setRipeti(ripeti);
		newState.setTempo(tempo);
		impiantoTemporizzato(newState);
	}

	@Override
	public void impiantoTemporizzato(WaterState water) {
		repository.save(water);
		logger.info("******Azionamento schedulato******");
		logger.info("Inizio alle ore =" + water.getDataInizio());
		logger.info("Tempo di esercizio =" + water.getTempo());
		logger.info("**********************************");
		logger.info("");

	}
}
