package it.irrigazione.raspberry.lp.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.irrigazione.raspberry.lp.model.WaterState;

@Repository
public interface RepositoryIrr extends CrudRepository<WaterState, Long> {

}
