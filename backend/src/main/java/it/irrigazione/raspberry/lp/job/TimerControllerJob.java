package it.irrigazione.raspberry.lp.job;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import it.irrigazione.raspberry.lp.service.QuartzService;
import it.irrigazione.raspberry.lp.service.impl.IrrigazioneServiceImpl;

public class TimerControllerJob implements Job {

	
	@Autowired
	QuartzService service;
	
	private static final Logger logger = LogManager.getLogger(IrrigazioneServiceImpl.class);

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {

		logger.debug("******Controllo interventi schedulati*****");
		
		service.QuartzController();	
	}


}
