package it.irrigazione.raspberry.lp.quartz.configuration;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.SimpleTrigger;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.scheduling.quartz.SimpleTriggerFactoryBean;
import org.springframework.scheduling.quartz.SpringBeanJobFactory;
import it.irrigazione.raspberry.lp.job.TimerControllerJob;


@Configuration
public class ConfigureQuartz {
	
	@Autowired
	private ApplicationContext applicationContext;

	public static SimpleTriggerFactoryBean createTrigger(JobDetail jobDetail, long pollFrequencyMs) {
		SimpleTriggerFactoryBean factoryBean = new SimpleTriggerFactoryBean();
		factoryBean.setJobDetail(jobDetail);
		factoryBean.setStartDelay(0L);
		factoryBean.setRepeatInterval(pollFrequencyMs);
		factoryBean.setRepeatCount(SimpleTrigger.REPEAT_INDEFINITELY);
		factoryBean.setMisfireInstruction(SimpleTrigger.MISFIRE_INSTRUCTION_RESCHEDULE_NEXT_WITH_REMAINING_COUNT);
		return factoryBean;
	}

	public static JobDetailFactoryBean createJobDetail(Class<?> jobClass) {
		JobDetailFactoryBean factoryBean = new JobDetailFactoryBean();
		factoryBean.setJobClass(jobClass);
		factoryBean.setDurability(true);
		return factoryBean;
	}
	
	
	@Bean
	public JobDetail jobDetail() {
	    return JobBuilder.newJob().ofType(TimerControllerJob.class)
	      .storeDurably()
	      .withIdentity("timerController", "group1")  
	      .withDescription("Timer Controllo Sistemi")
	      .build();
	}
	

	@Bean
	public Trigger trigger(JobDetail job) {
	    return TriggerBuilder.newTrigger().forJob(job)
	    		.withIdentity("timerControllerTrigger", "group1")
				.withSchedule(
					CronScheduleBuilder.cronSchedule("0/5 * * * * ?"))
				.build();
	}
	
	@Bean
	public SchedulerFactoryBean scheduler(Trigger trigger, JobDetail job) {
	    SchedulerFactoryBean schedulerFactory = new SchedulerFactoryBean();
	    schedulerFactory.setConfigLocation(new ClassPathResource("quartz.properties"));
	    schedulerFactory.setJobFactory(springBeanJobFactory());
	    schedulerFactory.setJobDetails(job);
	    schedulerFactory.setTriggers(trigger);
	    return schedulerFactory;
	}
	
	@Bean
	public SpringBeanJobFactory springBeanJobFactory() {
	    AutowiringSpringBeanJobFactory jobFactory = new AutowiringSpringBeanJobFactory();
	    jobFactory.setApplicationContext(applicationContext);
	    return jobFactory;
	}	
}
