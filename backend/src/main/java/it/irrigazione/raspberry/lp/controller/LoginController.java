package it.irrigazione.raspberry.lp.controller;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class LoginController {
	
	@PostMapping(value = "/login")
	public String login(@RequestParam(value = "impianto1", required = false) Boolean imp1,
			@RequestParam(value = "impianto2", required = false) Boolean imp2,
			@RequestParam("dataInizio") @DateTimeFormat(pattern="yyyy-MM-dd'T'HH:mm") Date data,
			@RequestParam(value = "ripeti", required = false) Boolean rip,
			@RequestParam("tempo") int tempo
			) {
		return "index";
	}

}
