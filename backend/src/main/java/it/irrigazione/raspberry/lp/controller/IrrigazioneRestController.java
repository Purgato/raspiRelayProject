package it.irrigazione.raspberry.lp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.pi4j.wiringpi.Gpio;
import it.irrigazione.raspberry.lp.service.IrrigazioneService;

@RestController
public class IrrigazioneRestController {
	

	@Autowired
	IrrigazioneService irrigazioneService;
	
	String stato = "";
	


	@GetMapping("/impianto1Fe")
	public String switchImpianto1FrontEnd(Model model){
		irrigazioneService.switchImpianto1();
		if(Gpio.digitalRead(1) == 1) stato = "0";
		if(Gpio.digitalRead(1) == 0) stato = "1";
		model.addAttribute("impianto1", stato);
		return stato;
	}
	
	@GetMapping("/impianto2Fe")
	public String switchImpianto2FrontEnd(Model model){
		irrigazioneService.switchImpianto2();
		if(Gpio.digitalRead(4) == 1) stato = "0";
		if(Gpio.digitalRead(4) == 0) stato = "1";
		model.addAttribute("impianto2", stato);
		return stato;
	}
	
	

	
}
