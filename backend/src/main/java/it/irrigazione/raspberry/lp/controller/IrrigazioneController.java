package it.irrigazione.raspberry.lp.controller;

import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.wiringpi.Gpio;
import it.irrigazione.raspberry.lp.model.WaterState;
import it.irrigazione.raspberry.lp.service.IrrigazioneService;

@Controller
public class IrrigazioneController {
	

	@Autowired
	IrrigazioneService irrigazioneService;
	
	String stato = "ok";
	
	/////////////////// PAGINE MAPPATE //////////////////////
	@GetMapping("/index.html")
	public String indexHtml(){ return "redirect:/";}
	@GetMapping("/forms-advanced.html")
	public String form(Model model){return "forms-advanced";}
	@GetMapping("/page-login.html")
	public String login(){ return "page-login";}
	@GetMapping("/page-register.html")
	public String register(){ return "page-register.html";}
	@GetMapping("/page-forget.html")
	public String forget(){ return "page-forget";}
	////////////////////////////////////////////////////////
	
	@GetMapping("/")
	public String index(Model model)
	{
		List<GpioPinDigitalOutput> state = irrigazioneService.initializeRaspberry();
		GpioPinDigitalOutput pin1 = state.get(0);
		GpioPinDigitalOutput pin2= state.get(1);
		
		if(pin2==null && pin1==null) {
			model.addAttribute("interruttore1", 0);
			model.addAttribute("interruttore2", 0);
		}
		else {
			if (Gpio.digitalRead(1) == 1) {
				model.addAttribute("interruttore1", 0);
			}
			else {
				model.addAttribute("interruttore1", 1);
			}
			if (Gpio.digitalRead(4) == 1) {
				model.addAttribute("interruttore2", 0);
			}
			else {
				model.addAttribute("interruttore2", 1);
			}
		}
		
		List<WaterState> list = irrigazioneService.listaTot();
		model.addAttribute("list", list);
		
		return "index";
	}

	@PostMapping(value = "/input")
	public String form(
			@RequestParam(value = "impianto1", required = false) Boolean imp1,
			@RequestParam(value = "impianto2", required = false) Boolean imp2,
			@RequestParam("dataInizio") @DateTimeFormat(pattern="yyyy-MM-dd'T'HH:mm") Date data,
			@RequestParam(value = "ripeti", required = false) Boolean rip,
			@RequestParam("tempo") int tempo
			) {
		if (imp1 == null) imp1=false;
		if (imp2 == null) imp2=false;
		if (rip == null) rip=false;
		int impianto1 =  imp1 ? 1 : 0;
		int impianto2 =  imp2 ? 1 : 0;
		String ripeti = rip ? "On" : "Off";
		
		irrigazioneService.createSchedulazione(impianto1, impianto2, data, ripeti, tempo);
		return "redirect:forms-advanced.html";
	}
	
	@GetMapping("/waterstateDelete")
	public String delete(@RequestParam(name="Id") String id){
		System.out.println(id);
		irrigazioneService.eliminaSchedulazione(new Long(id));
		return "redirect:/";
	}
}
