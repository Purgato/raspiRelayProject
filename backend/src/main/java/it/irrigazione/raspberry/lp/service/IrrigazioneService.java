package it.irrigazione.raspberry.lp.service;

import java.util.Date;
import java.util.List;

import com.pi4j.io.gpio.GpioPinDigitalOutput;

import it.irrigazione.raspberry.lp.model.WaterState;

public interface IrrigazioneService {
	
	public abstract List<GpioPinDigitalOutput> initializeRaspberry();
	
	public abstract void azionaImpianto(WaterState water);
	
	public abstract void spegniImpianto(WaterState water);
	
	public abstract void switchImpianto1();
	
	public abstract void switchImpianto2();
	
	public abstract List<WaterState> listaTot();

	public abstract void eliminaSchedulazione(Long id);

	public abstract void createSchedulazione(int impianto1, int impianto2, Date data, String ripeti, int tempo);
	
	public abstract void impiantoTemporizzato(WaterState water);

}
