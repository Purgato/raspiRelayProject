package it.irrigazione.raspberry.lp;

import java.text.ParseException;
import org.quartz.SchedulerException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories("it.irrigazione.raspberry.lp.repository")
@EntityScan(basePackages = {"it.irrigazione.raspberry.lp.model"}) 
public class IrrigazioneApplication {

	public static void main(String[] args) throws ParseException, SchedulerException {
		SpringApplication.run(IrrigazioneApplication.class, args);
	}
}
