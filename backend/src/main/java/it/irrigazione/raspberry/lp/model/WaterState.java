package it.irrigazione.raspberry.lp.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;


@Entity
@Table(name="waterstate")
public class WaterState {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long idn;
	
	@NotNull
	private int impianto1;
	
	@NotNull
	private int impianto2;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")	
	private Date dataInizio;

	private int tempo; 
	
	@NotNull
	private String ripeti;

	public int getImpianto1() {
		return this.impianto1;
	}
	
	public void setImpianto1(int impianto1) {
		this.impianto1 = impianto1;
	}

	public int getImpianto2() {
		return impianto2;
	}

	public void setImpianto2(int impianto2) {
		this.impianto2 = impianto2;
	}

	public Date getDataInizio() {
		return dataInizio;
	}

	public void setDataInizio(Date dataInizio) {
		this.dataInizio = dataInizio;
	}

	public int getTempo() {
		return tempo;
	}

	public void setTempo(int tempo) {
		this.tempo = tempo;
	}

	public long getIdn() {
		return idn;
	}

	public void setIdn(long idn) {
		this.idn = idn;
	}

	public String getRipeti() {
		return ripeti;
	}

	public void setRipeti(String ripeti) {
		this.ripeti = ripeti;
	}


}
