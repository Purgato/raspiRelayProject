package it.irrigazione.raspberry.lp.service.impl;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import it.irrigazione.raspberry.lp.model.WaterState;
import it.irrigazione.raspberry.lp.repository.RepositoryIrr;
import it.irrigazione.raspberry.lp.service.IrrigazioneService;
import it.irrigazione.raspberry.lp.service.QuartzService;

@Service
public class QuartzServiceImpl implements QuartzService {


	@Autowired
	RepositoryIrr repository;
	
	@Autowired
	IrrigazioneService service;
	
	private static final Logger logger = LogManager.getLogger(QuartzServiceImpl.class);


	@Override
	public void QuartzController() {

		List<?> waterState = (List<WaterState>) repository.findAll();
		Date now = new Date();
		for (Object object : waterState) {
			if (object instanceof WaterState) {
				WaterState w = (WaterState) object;
				Date dataInizio = w.getDataInizio();
				Date dataFine = addMinutesToDate(w.getTempo(), dataInizio);
				if (now.after(dataInizio) && now.before(dataFine)) {
					Runnable subtask = () -> {
						repository.save(w);
						System.out.println("******IMPIANTO AZIONATO******" + new Timestamp(new Date().getTime()));
						logger.info("******IMPIANTO AZIONATO******" + new Timestamp(new Date().getTime()));
						service.azionaImpianto(w);
					};
					ScheduledExecutorService sessione = Executors.newScheduledThreadPool(1); // nuovo thread
					sessione.schedule(subtask, 1, TimeUnit.MILLISECONDS);
				} else if (now.after(dataFine)) {
					service.spegniImpianto(w);
					System.out.println("******IMPIANTO SPENTO******" + new Timestamp(new Date().getTime()));
					logger.info("******IMPIANTO SPENTO******" + new Timestamp(new Date().getTime()));
					if (w.getRipeti().equals("Si")) {
						w.setDataInizio(addDays(w.getDataInizio(), 1));
						repository.save(w);
					} else {
						repository.delete(new Long(w.getIdn()));
					}
				}

			}
		}
	}
	
	private static Date addMinutesToDate(int minutes, Date beforeTime) {
		final long ONE_MINUTE_IN_MILLIS = 60000;
		long curTimeInMs = beforeTime.getTime();
		Date afterAddingMins = new Date(curTimeInMs + (minutes * ONE_MINUTE_IN_MILLIS));
		return afterAddingMins;
	}

	public Date addDays(Date date, int days) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, days);
		return cal.getTime();
	}
}
