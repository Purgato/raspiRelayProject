$.noConflict();

jQuery(document).ready(function ($) {

	"use strict";

	[].slice.call(document.querySelectorAll('select.cs-select')).forEach(function (el) {
		new SelectFx(el);
	});

	jQuery('.selectpicker').selectpicker;


	$('#menuToggle').on('click', function (event) {
		$('body').toggleClass('open');
	});

	$('.search-trigger').on('click', function (event) {
		event.preventDefault();
		event.stopPropagation();
		$('.search-trigger').parent('.header-left').addClass('open');
	});

	$('.search-close').on('click', function (event) {
		event.preventDefault();
		event.stopPropagation();
		$('.search-trigger').parent('.header-left').removeClass('open');
	});

	
	$(document).ready(function () {

		$('#impianto1Button').click(function () {

			$.ajax({
				type: 'GET',
				dataType: 'text',
				url: '/impianto1Fe',
				error: function (error) {
					console.log('Error ${error}')
				}

			});
			
			$('#impianto2Button').click(function () {

				$.ajax({
					type: 'GET',
					dataType: 'text',
					url: '/impianto2Fe',

					error: function (error) {
						console.log('Error ${error}')
					}

				});

		});
	});



});



