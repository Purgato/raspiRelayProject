$.noConflict();

jQuery(document).ready(function($) {

	"use strict";

	[].slice.call( document.querySelectorAll( 'select.cs-select' ) ).forEach( function(el) {
		new SelectFx(el);
	} );

	jQuery('.selectpicker').selectpicker;


	$('#menuToggle').on('click', function(event) {
		$('body').toggleClass('open');
	});

	$('.search-trigger').on('click', function(event) {
		event.preventDefault();
		event.stopPropagation();
		$('.search-trigger').parent('.header-left').addClass('open');
	});

	$('.search-close').on('click', function(event) {
		event.preventDefault();
		event.stopPropagation();
		$('.search-trigger').parent('.header-left').removeClass('open');
	});

	// $('.user-area> a').on('click', function(event) {
	// 	event.preventDefault();
	// 	event.stopPropagation();
	// 	$('.user-menu').parent().removeClass('open');
	// 	$('.user-menu').parent().toggleClass('open');
	// });


$(document).ready(function(){

	$('#impianto1Button').click(function(){
		
		$.ajax({ 
			type: 'GET', 
   			dataType: 'text',
			url: 'http://localhost:9999/impianto1Fe', 
			success: function (data) { 
				console.log(data)
			},
			error:function(error){
				console.log('Error ${error}')
			}

		});
	});
});

$(document).ready(function(){

	$('#impianto2Button').click(function(){
		
		$.ajax({ 
			type: 'GET', 
   			dataType: 'text',
			url: 'http://localhost:9999/impianto2Fe', 
			success: function (data) { 
				console.log(data)
			},
			error:function(error){
				console.log('Error ${error}')
			}

		});
	});
});


});